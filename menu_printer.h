/*
 * Drawing menu for user input.
 * author: Dusan Jansky, Martin Havlicek
 */


#ifndef MENU_PRINTER_HEADER
#define MENU_PRINTER_HEADER


/*
 * clearing unused lines from menu in console
 */
void clear_lines(int height);


/*
 * printing menus for selected devices
 */
void print_module_menu();
void print_core_menu();
void print_o2_menu();
void print_co2_menu();
void print_temp_menu();
void print_press_menu();
void print_airlock_menu();

/*
 * calling specific menu print function
 */
void print_menu();

/*
 * changing menu and settings of devices based on input
 */
void update_menu(data_t data, char input);

#endif
