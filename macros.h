/*
 * Global constants and macros
 * authors: Dusan Jansky, Martin havlicek
 */


#ifndef MACROS_HEADER
#define MACROS_HEADER

// macros
#define rotate_cord_x(angle, x, y) ((x * cos(angle)) - (y * sin(angle)))
#define rotate_cord_y(angle, x, y) ((x * sin(angle)) + (y * cos(angle)))

#define get_percentage_value(value, min, max) (((value - min) * 100)/(max - min))

// constants
#define COLOR_MODULE		0xffff
#define COLOR_MODULE_ALERT	0xfde0
#define COLOR_UNITS_PRIM	0xd882	
#define COLOR_UNITS_SEC		0x0371
#define COLOR_BLACK			0x0000
#define COLOR_RED			0xd882

#endif
