/*
 * Printing infomation code messages to console and display
 * author: Dusan Jansky, Martin Havlicek
 */

#ifndef MESSAGES_HEADER
#define MESSAGES_HEADER

#include <stdio.h>
#include <stdbool.h>

#include "struct.h"


/*
 * updates console array and adding new message
 */
void print_console(char *string, uint16_t color, console_t con);

/*
 * printing any string to console without braking menu drawing
 */
void print_msg(char *string, bool enable_color, int color);

/*
 * prints specific code message to terminal and optionaly to display
 */
void print_code(int module_id, int msg_id, bool draw_cons, data_t data);

#endif
