/*
 * Calculates new values for space ship
 * author: Dusan Jansky, Martin Havlicek
 */

#ifndef TASK_PROCESSOR_HEADER
#define TASK_PROCESSOR_HEADER

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "struct.h"
#include "macros.h"

/*
 * functions for input processing
 */
int select_status(char in_char);
int select_device(char in_char);

/*
 * init default values for space ship devices
 */
void set_default_values(data_t* data);


/*
 * Turn off random device
 */
void turn_off_rand_device(data_t data);

/*
 * updates values in data structure acording to values in task processor file
 */
void update_values(data_t* data);

/*
 * set device status
 */
void set_device(data_t data, int device, int status);

/*
 * calculates new values
 */
void modify_values();

/*
 * called from process event in events.h
 */
void do_event_nebula_ice();
void do_event_nebula_elc();
void do_event_star();
void do_event_black_hole();

/*
 * chceck if player still alive. Ret true when alive, false otherwise.
 */
bool alive();


#endif
