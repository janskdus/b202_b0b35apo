/*
 * Drawing window modules to memory
 * author: Dusan Jansky, Martin havlicek
 */

#ifndef MODULES_GAME_HEADER
#define MODULES_GAME_HEADER

#include <stdint.h>

#include "struct.h"
#include "font_types.h"


// note: param id = window number stored in memory

/*
 * writes col pixel values to data structure
 */
void draw_column(double value, double raw_value, data_t data, int id, font_descriptor_t *fdes);


/*
 * writes square pixel values to data structure
 */
void draw_square(double value, double raw_value, data_t data, int id, font_descriptor_t *fdes);


/*
 * writes console pixel values to data structure
 */
void draw_console(data_t data, int id, font_descriptor_t *fdes);


/*
 * writes dead overlay pixel values to data structure
 */
void draw_dead_overlay(data_t data, int id, font_descriptor_t *fdes);


#endif
