/*
 * Sets global parameters data structure
 * authors: Dusan Jansky, Martin havlicek
 */

#ifndef STRUCT_HEADER
#define STRUCT_HEADER

#include <stdio.h>
#include <string.h>
#include <stdbool.h>

typedef struct
{
	double value;
	double min;
	double max;
} displayed_vals_t;

typedef struct
{
	char *string;
	uint16_t color;
} message_t;

typedef struct
{
	message_t *msg;
	int len;
	int rows;
	int text_size;
} console_t;

typedef struct
{
	int start_x;
	int start_y;
	int width;
	int height;
} window_cnf_t;

typedef struct
{
	char* name;
	int type;
	window_cnf_t cnf;
} windows_t;

typedef struct 
{
	uint16_t *display;
	displayed_vals_t *values;
	console_t console;
	windows_t *window;
	bool dead;
	unsigned long int life_cycles;
} data_t;

/*
 * data structure init
 */
data_t set_data();

#endif // end of struct.h
