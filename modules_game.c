/*
 * Drawing window modules to memory
 * author: Dusan Jansky, Martin havlicek
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <stdbool.h>
#include <math.h>
#include <string.h>

#include "macros.h"
#include "struct.h"
#include "mzapo_parlcd.h"
#include "font_types.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "lcd_screen.h"


void draw_column(double value, double raw_value, data_t data, int id, font_descriptor_t *fdes) {
	windows_t win = data.window[id];
	window_cnf_t cnf = win.cnf;

	//col parameters
	int main_offset = 10;
	int top_offset = 20;
	int bottom_offset = 20;
	int name_text_x = 25;
	int name_text_y = 15;
	double name_text_size = 2;
	int value_text_x = 25;
	int value_text_y = 302;
	double value_text_size = 1.5;

	int name_text_width;
	bool *name_text = get_string_field(name_text_size, fdes, win.name, &name_text_width);
	char value_string[6];
	snprintf(value_string, 6, "%3.1f", raw_value);
	int value_text_width;
	bool *value_text = get_string_field(value_text_size, fdes, value_string, &value_text_width);

	int value_bar_height = cnf.height - main_offset * 2 - top_offset - bottom_offset;
	int unfilled_bar_height = (100 - value) * ((double)value_bar_height / 100);

	for (int h = 0; h < cnf.height; h++){
		for (int w = 0; w < cnf.width; w++){
			if (h > main_offset + top_offset &&
				h < cnf.height - main_offset - bottom_offset &&
				w > main_offset && w < cnf.width - main_offset) {
				if (h < unfilled_bar_height + main_offset + top_offset) {
					data.display[(h+cnf.start_y)*480+w+cnf.start_x] = COLOR_BLACK;
				} else {
					data.display[(h+cnf.start_y)*480+w+cnf.start_x] = COLOR_UNITS_PRIM;
				}
			} else {
				if (value < 10 || value > 90) {
					data.display[(h+cnf.start_y)*480+w+cnf.start_x] = COLOR_MODULE_ALERT;
				} else {
					data.display[(h+cnf.start_y)*480+w+cnf.start_x] = COLOR_MODULE;
				}
			}

			// module name text
			if (w > name_text_x-name_text_width/2 &&
				w < name_text_x+name_text_width/2 &&
				h > name_text_y-name_text_size*8 &&
				h < name_text_y+name_text_size*8) {
				int idxx = w-name_text_x-name_text_width/2;
				int idxy = h-name_text_y+name_text_size*8;
				if (name_text[idxy*name_text_width+idxx])
					data.display[(h+cnf.start_y)*480+w+cnf.start_x] = COLOR_BLACK;
			}

			// module value text
			if (w > value_text_x-value_text_width/2 &&
				w < value_text_x+value_text_width/2 &&
				h > value_text_y-value_text_size*8 &&
				h < value_text_y+value_text_size*8) {
				int idxx = w-value_text_x-value_text_width/2;
				int idxy = h-value_text_y+value_text_size*8;
				if (value_text[idxy*value_text_width+idxx])
					data.display[(h+cnf.start_y)*480+w+cnf.start_x] = COLOR_BLACK;
			}
		}
	}
	
	if (name_text != NULL)
		free(name_text);
	if (value_text != NULL)
		free(value_text);

	return;
}


void draw_square(double value, double raw_value, data_t data, int id, font_descriptor_t *fdes) {
	double distance;
	double angle_calc;
	double angle_calc_filled;
	int calc_h, calc_w;
	windows_t win = data.window[id];
	window_cnf_t cnf = win.cnf;

	//square parameters
	int circle_h_offset = 30;
	int inner_r = 25;
	int outer_r = 45;
	int max_angle = 2;
	double offset_angle = 3.14;
	int name_text_x = 50;
	int name_text_y = 20;
	double name_text_size = 2.5;
	int value_text_x = 50;
	int value_text_y = 80;
	double value_text_size = 2;
	
	int name_text_width;
	bool *name_text = get_string_field(name_text_size, fdes, win.name, &name_text_width);
	char value_string[6];
	snprintf(value_string, 6, "%3.1f", raw_value);
	int value_text_width;
	bool *value_text = get_string_field(value_text_size, fdes, value_string, &value_text_width);

	double value_angle = value * max_angle / 100;
	double value_offset_angle = (max_angle - value_angle) / 2;
	
	for (int h = 0; h < cnf.height; h++) {
		for (int w = 0; w < cnf.width; w++) {
			calc_w = rotate_cord_x(offset_angle, w, h) + cnf.width/2;
			calc_h = rotate_cord_y(offset_angle, w, h) + cnf.height/2 + circle_h_offset;
	
			distance = pow(calc_h, 2.0) + pow(calc_w, 2.0);
			angle_calc = abs(calc_w/tan(max_angle/2));
			angle_calc_filled = abs(rotate_cord_x(value_offset_angle, calc_w, calc_h)/tan(value_angle/2));

			if (pow(outer_r, 2.0) > distance && pow(inner_r, 2.0) < distance && calc_h > angle_calc) {
				if (rotate_cord_y(value_offset_angle, calc_w, calc_h) > angle_calc_filled) {
					data.display[(h+cnf.start_y)*480+w+cnf.start_x] = COLOR_UNITS_PRIM;
				} else {
					data.display[(h+cnf.start_y)*480+w+cnf.start_x] = COLOR_BLACK;
				}
			} else {
				if (value < 10 || value > 90) {
					data.display[(h+cnf.start_y)*480+w+cnf.start_x] = COLOR_MODULE_ALERT;
				} else {
					data.display[(h+cnf.start_y)*480+w+cnf.start_x] = COLOR_MODULE;
				}
			}

			// module name text
			if (w > name_text_x-name_text_width/2 &&
				w < name_text_x+name_text_width/2 &&
				h > name_text_y-name_text_size*8 &&
				h < name_text_y+name_text_size*8) {
				int idxx = w-name_text_x-name_text_width/2;
				int idxy = h-name_text_y+name_text_size*8;
				if (name_text[idxy*name_text_width+idxx])
					data.display[(h+cnf.start_y)*480+w+cnf.start_x] = COLOR_BLACK;
			}

			// module value text
			if (w > value_text_x-value_text_width/2 &&
				w < value_text_x+value_text_width/2 &&
				h > value_text_y-value_text_size*8 &&
				h < value_text_y+value_text_size*8) {
				int idxx = w-value_text_x-value_text_width/2;
				int idxy = h-value_text_y+value_text_size*8;
				if (value_text[idxy*value_text_width+idxx])
					data.display[(h+cnf.start_y)*480+w+cnf.start_x] = COLOR_BLACK;
			}
		}
	}
	if (name_text != NULL)
		free(name_text);
	if (value_text != NULL)
		free(value_text);

	return;
}

void draw_console(data_t data, int id, font_descriptor_t *fdes) {
	console_t console = data.console;
	window_cnf_t cnf = data.window[id].cnf;
	int written_rows = 0;
	int msg_text_width;
	bool *msg_text = NULL;

	for (int msg = 0; msg < console.rows; msg++) {
		msg_text = get_string_field(console.text_size, fdes, console.msg[msg].string, &msg_text_width);

		for (int h = 0; h < 16 * console.text_size; h++) {
			for (int w = 0; w < cnf.width; w++) {
				if (written_rows + h > cnf.height) break;

				data.display[(h+written_rows+cnf.start_y)*480+w+cnf.start_x] = 0x528a;

				if (w > 0 && w < msg_text_width &&
					h > 0 && h < console.text_size*16) {
					if (msg_text[h*msg_text_width+w])
						data.display[(h+written_rows+cnf.start_y)*480+w+cnf.start_x] = console.msg[msg].color;
				}
			}
		}
		written_rows+=16*console.text_size;
	}
	
	if (msg_text != NULL) {
		free(msg_text);
	}

	return;
}

void draw_dead_overlay(data_t data, int id, font_descriptor_t *fdes) {
	windows_t win = data.window[id];
	window_cnf_t cnf = win.cnf;
	unsigned long int cycles = data.life_cycles;

	int alive_time_mS = cycles % 10;
	int alive_time_H = cycles / 36000;
	cycles -= alive_time_H * 36000;
	int alive_time_M = cycles / 600;
	cycles -= alive_time_M * 600;
	int alive_time_S = cycles / 10;

	char *time_str = malloc(sizeof(char)*32);
	char temp_str[28];

	strcat(time_str, "You were alive for ");
	sprintf(temp_str, "%02d", alive_time_H);
	strcat(time_str, temp_str);
	strcat(time_str, ":");
	sprintf(temp_str, "%02d", alive_time_M);
	strcat(time_str, temp_str);
	strcat(time_str, ":");
	sprintf(temp_str, "%02d", alive_time_S);
	strcat(time_str, temp_str);
	strcat(time_str, ".");
	sprintf(temp_str, "%01d", alive_time_mS);
	strcat(time_str, temp_str);
	strcat(time_str, ".");

	//text parameters
	int time_text_x = 240;
	int time_text_y = 74;
	double time_text_size = 2;
	char text_str[] = "You are dead.";
	int main_text_x = 240;
	int main_text_y = 34;
	double main_text_size = 3;
	
	int time_text_width;
	bool *time_text = get_string_field(time_text_size, fdes, time_str, &time_text_width);
	int main_text_width;
	bool *main_text = get_string_field(main_text_size, fdes, text_str, &main_text_width);

	for (int h = 0; h < cnf.height; h++) {
		for (int w = 0; w < cnf.width; w++) {
			data.display[(h+cnf.start_y)*480+w+cnf.start_x] = COLOR_RED;

			// module main text
			if (w > main_text_x-main_text_width/2 &&
				w < main_text_x+main_text_width/2 &&
				h > main_text_y-main_text_size*8 &&
				h < main_text_y+main_text_size*8) {
				int idxx = w-main_text_x-main_text_width/2;
				int idxy = h-main_text_y+main_text_size*8;
				if (main_text[idxy*main_text_width+idxx])
					data.display[(h+cnf.start_y)*480+w+cnf.start_x] = COLOR_MODULE;
			}

			// module time text
			if (w > time_text_x-time_text_width/2 &&
				w < time_text_x+time_text_width/2 &&
				h > time_text_y-time_text_size*8 &&
				h < time_text_y+time_text_size*8) {
				int idxx = w-time_text_x-time_text_width/2;
				int idxy = h-time_text_y+time_text_size*8;
				if (time_text[idxy*time_text_width+idxx])
					data.display[(h+cnf.start_y)*480+w+cnf.start_x] = COLOR_MODULE;
			}
		}
	}

	if (main_text != NULL)
		free(main_text);

	return;
}
