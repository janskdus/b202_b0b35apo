/*
 * Calculates new values for space ship
 * author: Dusan Jansky, Martin Havlicek
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "struct.h"
#include "macros.h"
#include "messages.h"

// ----------------------------
#define VOLUME_SHIP 25000
#define AMP_OFF_UNIT 10

#define O2_START (0.21*VOLUME_SHIP)
#define CO2_START (0.013*VOLUME_SHIP)
#define N_START (VOLUME_SHIP-(O2_START+CO2_START))

// UNITS
// CORE_GEN
#define UNIT_AMP_ON_INCREASE 0.08
#define UNIT_AMP_OFF 0.005
#define UNIT_AMP_DEVICE 0.07

// TEMP_GEN
#define UNIT_TEMP_ON 0.06
#define UNIT_TEMP_OFF 0.03
#define UNIT_TEMP_AIRLOCK (5*UNIT_TEMP_ON)

// GASS UNITS
#define UNIT_O2_VOLUME (0.00125*VOLUME_SHIP)
#define UNIT_CO2_VOLUME (0.000075*VOLUME_SHIP)
#define UNIT_N_VOLUME (0.01*VOLUME_SHIP)


// WARNING BORDER VALUES
#define AMP_HIGH_WARNING 90 
#define AMP_LOW_WARNING 10
#define O2_HIGH_WARNING 30
#define O2_LOW_WARNING 10
#define CO2_HIGH_WARNING 5
#define CO2_LOW_WARNING 0
#define TEMP_HIGH_WARNING 42
#define TEMP_LOW_WARNING 16
#define PRESS_HIGH_WARNING 90
#define PRESS_LOW_WARNING 60


#define DIE_TIMER 200


enum idx_data_values { AMP, TEMP, PRESS, O2, CO2, N };
enum modules { CORE_GEN = 1, O2_GEN, CO2_GEN, TEMP_GEN, PRESS_GEN, AIRLOCK };
enum idx_states { ON_INCREASE = 1, ON_DECREASE, OFF };

double amp, temp, o2_volume, co2_volume, n_volume;
int* devices;
bool* warnings;

int* alive_atrib;

int select_status(char in_char){
	char tmp[2] = {in_char, '\0'};
	int ret = 101;
	int status = atoi(tmp);
	if (status == ON_DECREASE || status == ON_INCREASE || status == OFF){
		ret = status;
	}
	return ret;
}

int select_device(char in_char){
	char tmp [2] = {in_char, '\0'};
	int ret = 101;
	int device = atoi(tmp);
	if (device == CORE_GEN || device == O2_GEN || device == CO2_GEN 
		|| device == TEMP_GEN || device ==  PRESS_GEN || device == AIRLOCK)
		ret = device;
	return ret;
}


void set_default_values(data_t* data){
	devices = malloc(sizeof(int)*6);
	warnings = malloc(sizeof(bool)*6);
	alive_atrib = malloc(sizeof(int)*5);

	if (!warnings || !devices) {
		perror("Alloc error: ");
		exit(EXIT_FAILURE);
	}
	for (int i = 0; i < 6; ++i){
		devices[i] = OFF;
		warnings[i] = false;
	}
	for (int i = 0; i < 5; ++i){
		alive_atrib[i] = 0;
	}

	amp = data->values[AMP].value;
	temp = data->values[TEMP].value;
	o2_volume = O2_START;
	co2_volume = CO2_START;
	n_volume = N_START;
}


void modify_values(){
	switch (devices[CORE_GEN]) {
		case ON_INCREASE:
			amp += UNIT_AMP_ON_INCREASE;
			temp += 1.5 * UNIT_TEMP_OFF;
			break;
		case ON_DECREASE:
		case OFF:
			amp += UNIT_AMP_OFF;
			break;
		default:
			devices[CORE_GEN] = OFF;
	}
	switch (devices[TEMP_GEN]) {
		case ON_INCREASE:
			temp += UNIT_TEMP_ON;
			amp -= UNIT_AMP_DEVICE;
			break;
		case OFF:
			break;
		case ON_DECREASE:
			temp -= UNIT_TEMP_ON;
			amp -=UNIT_AMP_DEVICE;
			break;
		default:
			devices[TEMP_GEN] = OFF;
	}

	switch (devices[O2_GEN]) {
		case ON_INCREASE:
			o2_volume += UNIT_O2_VOLUME;
			amp -= UNIT_AMP_DEVICE;
			break;
		case OFF:
			break;
		case ON_DECREASE:
			o2_volume -= UNIT_O2_VOLUME;
			amp -=UNIT_AMP_DEVICE;
			break;
		default:
			devices[O2_GEN] = OFF;
	}
	switch (devices[CO2_GEN]) {
		case ON_INCREASE:
			co2_volume += UNIT_CO2_VOLUME;
			amp -=UNIT_AMP_DEVICE;
			break;
		case OFF:
			break;
		case ON_DECREASE:
			co2_volume -= UNIT_CO2_VOLUME;
			amp -=UNIT_AMP_DEVICE;
			break;
		default:
			devices[CO2_GEN] = OFF;
	}
	switch (devices[PRESS_GEN]) {

		case ON_INCREASE:
			n_volume += UNIT_N_VOLUME;
			amp -=UNIT_AMP_DEVICE;
			break;
		case OFF:
			break;
		case ON_DECREASE:
			n_volume -= UNIT_N_VOLUME;
			amp -=UNIT_AMP_DEVICE;
			break;
		default:
			devices[PRESS_GEN] = OFF;
	}
	switch (devices[AIRLOCK]) {
		case ON_INCREASE:
			o2_volume -= o2_volume / 2;
			co2_volume -= co2_volume / 2;
			n_volume -= n_volume / 2;
			temp -= UNIT_TEMP_AIRLOCK;
			break;
		case ON_DECREASE:
		case OFF:
			break;
		default:
			devices[AIRLOCK] = OFF;
	}
}

// this function should be called once per defined time
void update_values(data_t* data){
	

	modify_values();

	// TURNS OFF random device when low power
	if (amp <= UNIT_AMP_DEVICE) {
		turn_off_rand_device(data);
	}

	// breathing and low space temp costs
	temp -= UNIT_TEMP_OFF;
	o2_volume -= 0.00008 * VOLUME_SHIP;
	co2_volume += 0.00008 * VOLUME_SHIP;

	if (amp > data->values[AMP].max) amp = data->values[AMP].max;
	if (amp < data->values[AMP].min) amp = data->values[AMP].min;
	if (temp > data->values[TEMP].max) temp = data->values[TEMP].max;
	if (temp < data->values[TEMP].min) temp = data->values[TEMP].min;

	data->values[AMP].value = amp;
	data->values[TEMP].value = temp;
	data->values[PRESS].value = (o2_volume + co2_volume + n_volume) / VOLUME_SHIP;
	data->values[O2].value = 100* o2_volume / (o2_volume + co2_volume + n_volume);
	data->values[CO2].value = 100* co2_volume / (o2_volume + co2_volume + n_volume);
	data->values[N].value = 100* n_volume / (o2_volume + co2_volume + n_volume);
	

	// PRINT WARNING when value dangerous

	if (data->values[O2].value < O2_LOW_WARNING || data->values[O2].value > O2_HIGH_WARNING)
		alive_atrib[O2]++;
	else if (alive_atrib[O2] > 0)
		alive_atrib[O2]--;
	
	if (data->values[CO2].value > CO2_HIGH_WARNING)
		alive_atrib[CO2]++;
	else if (alive_atrib[CO2] > 0)
		alive_atrib[CO2]--;

	if (data->values[TEMP].value < TEMP_LOW_WARNING || data->values[TEMP].value > TEMP_HIGH_WARNING)
		alive_atrib[TEMP]++;
	else if (alive_atrib[TEMP] > 0)
		alive_atrib[TEMP]--;




	if (!warnings[O2] && data->values[O2].value < O2_LOW_WARNING)  {
		print_code(2, 14, true, *data);
		warnings[O2] = true;
	} else if (!warnings[O2] && data->values[O2].value > O2_HIGH_WARNING) {
		print_code(2, 15, true, *data);
		warnings[O2] = true;
	} else if (data->values[O2].value >= O2_LOW_WARNING && data->values[O2].value <= O2_HIGH_WARNING){
		warnings[O2] = false;
	}

	if (!warnings[CO2] && data->values[CO2].value > CO2_HIGH_WARNING) {
		print_code(3, 22, true, *data);
		warnings[CO2] = true;
	} else if (data->values[CO2].value >= CO2_LOW_WARNING && data->values[CO2].value <= CO2_HIGH_WARNING){
		warnings[CO2] = false;
	}

	if (!warnings[TEMP] && data->values[TEMP].value < TEMP_LOW_WARNING)  {
		print_code(4, 27, true, *data);
		warnings[TEMP] = true;
	} else if (!warnings[TEMP] && data->values[TEMP].value > TEMP_HIGH_WARNING) {
		print_code(4, 28, true, *data);
		warnings[TEMP] = true;
	} else if (data->values[TEMP].value >= TEMP_LOW_WARNING && data->values[TEMP].value <= TEMP_HIGH_WARNING){
		warnings[TEMP] = false;
	}

	if (!warnings[PRESS] && data->values[PRESS].value < PRESS_LOW_WARNING)  {
		print_code(5, 34, true, *data);
		warnings[PRESS] = true;
	} else if (!warnings[PRESS] && data->values[PRESS].value > PRESS_HIGH_WARNING) {
		print_code(5, 35, true, *data);
		warnings[PRESS] = true;
	} else if (data->values[PRESS].value >= PRESS_LOW_WARNING && data->values[PRESS].value <= PRESS_HIGH_WARNING){
		warnings[PRESS] = false;
	}

	if (!warnings[TEMP] && data->values[TEMP].value < TEMP_LOW_WARNING)  {
		print_code(4, 27, true, *data);
		warnings[TEMP] = true;
	} else if (!warnings[TEMP] && data->values[TEMP].value > TEMP_HIGH_WARNING) {
		print_code(4, 28, true, *data);
		warnings[TEMP] = true;
	} else if (data->values[TEMP].value >= TEMP_LOW_WARNING && data->values[TEMP].value <= TEMP_HIGH_WARNING){
		warnings[TEMP] = false;
	}
}



void set_device(data_t data, int device, int status) {
	
	// check if enough power to turn on device
	if (amp <= UNIT_AMP_DEVICE && device != CORE_GEN){
		switch(device){
			case O2_GEN:
				print_code(2, 17, true, data);
				break;
			case CO2_GEN:
				print_code(3, 23, true, data);
				break;
			case TEMP_GEN:
				print_code(4, 30, true, data);
				break;
			case PRESS_GEN:
				print_code(5, 37, true, data);
				break;
			default:
				break;
		}
		return;
	}
	switch(device){
		case CORE_GEN:
			if (status == ON_INCREASE){
				print_code(1, 6, true, data);
			} else {
				print_code(1, 7, true, data);
			}
			break;
		case O2_GEN:
			if (status == ON_INCREASE){
				print_code(2, 11, true, data);
			} else if (status == ON_DECREASE){
				print_code(2, 12, true, data);
			} else {
				print_code(2, 13, true, data);
			}
			break;
		case CO2_GEN:
			if (status == ON_INCREASE){
				print_code(3, 18, true, data);
			} else if (status == ON_DECREASE){
				print_code(3, 19, true, data);
			} else {
				print_code(3, 20, true, data);
			}
			break;
		case TEMP_GEN:
			if (status == ON_INCREASE){
				print_code(4, 24, true, data);
			} else if (status == ON_DECREASE){
				print_code(4, 25, true, data);
			} else {
				print_code(4, 26, true, data);
			}
			break;
		case PRESS_GEN:
			if (status == ON_INCREASE){
				print_code(5, 31, true, data);
			} else if (status == ON_DECREASE){
				print_code(5, 32, true, data);
			} else {
				print_code(5, 33, true, data);
			}
			break;
		case AIRLOCK:
			if (status == ON_INCREASE){
				print_code(6, 38, true, data);
			} else {
				print_code(6, 39, true, data);
			}
			break;
		default:
			break;
	}
	devices[device] = status;
}


void do_event_nebula_ice(){
	double multi_const = 0.9;
	temp *= multi_const;
}

void do_event_nebula_elc(){
	double multi_const = 0.9;
	amp *= multi_const;
}

void do_event_star(){
	double multi_const = 1.1;
	temp *= multi_const;
}

void do_event_black_hole(){
	const double multi_const = 1.1;	
	o2_volume *= multi_const;
	co2_volume *= multi_const;
	n_volume *= multi_const;
}

bool alive(){
	bool ret = true;
	for (int i = 0; i < 5; ++i){
		if (alive_atrib[i] >= DIE_TIMER) ret = false;
		
	}
	return ret;
}

void turn_off_rand_device(data_t data) {
	for (int i = O2_GEN; i <= PRESS_GEN; ++i){
		if (devices[i] == ON_INCREASE || devices[i] == ON_DECREASE){
			switch(i){
				case O2_GEN:
					print_code(2, 16, true, data);
					break;
				case CO2_GEN:
					print_code(3, 22, true, data);
					break;
				case TEMP_GEN:
					print_code(4, 29, true, data);
					break;
				case PRESS_GEN:
					print_code(5, 36, true, data);
					break;
				default:
					break;
			}
			devices[i] = OFF;
			break;
		}
	}
}
