/*
 * Basic drawing composition of larger elements on lcd
 * author: Dusan Jansky, Martin Havlicek
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <stdbool.h>
#include <math.h>

#include "macros.h"
#include "mzapo_parlcd.h"
#include "font_types.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "modules_game.h"
#include "lcd_screen.h"

void set_screen_black(unsigned char *parlcd_mem_base) {
	parlcd_write_cmd(parlcd_mem_base, 0x2c);
	for (int h = 0; h < 320 ; h++) {
		for (int w = 0; w < 480 ; w++) {
			parlcd_write_data(parlcd_mem_base, COLOR_BLACK);
		}
	}
}

void draw_whole_lcd(unsigned char* parlcd_mem_base, data_t* data){
	
 	if (data == NULL) {
		printf("NULL param spotted in function.\n");
		return;
	}
	
	font_descriptor_t* fdes = &font_winFreeSystem14x16;

	// reading display layout
	double percentage = 0;
	for (int i = 0; i < 7; i++) {
		switch(data->window[i].type) {
			case 1:
				percentage = get_percentage_value(data->values[i].value, data->values[i].min, data->values[i].max);
				draw_square(percentage, data->values[i].value, *data, i, fdes);
				break;
			case 2:
				percentage = get_percentage_value(data->values[i].value, data->values[i].min, data->values[i].max);
				draw_column(percentage, data->values[i].value, *data, i, fdes);
				break;
			case 3:
				draw_console(*data, i, fdes);
				break;
			default:
				break;

		}
	}

	if (data->dead) {
		draw_dead_overlay(*data, 7, fdes);
	}

	parlcd_write_cmd(parlcd_mem_base, 0x2c);
	for (int h = 0; h < 320; h++) {
		for (int w = 0; w < 480; w++) {
			parlcd_write_data(parlcd_mem_base, data->display[h*480+w]);
		}
	}
	return;
}


bool* get_string_field(double scaling_factor, font_descriptor_t* fdes, char *str, int *width) {
	const int height = 16;
	bool* arr;
	int size_str = strlen(str);
	*width = 0;
	int width_written = 0;
	int char_width = 0;
	int ch = 0;

	for (int i = 0; i < size_str; i++) {
		*width += get_char_width(fdes, (int)str[i]) * scaling_factor;
	}
	arr= (bool*)malloc(scaling_factor * (*width) * height);

	for (int i = 0; i < size_str; i++) {
		ch = (int)str[i]-32;
		char_width = get_char_width(fdes, ch+32);
		for (int h = 0; h < height*scaling_factor; h++) {
			uint16_t temp_row = fdes->bits[ch*fdes->height+(int)round(h/scaling_factor)];

			for (int w = char_width*scaling_factor; w > 0; w--) {
				int bits = (int) temp_row >> (16-(int)round(w/scaling_factor));
				arr[h*(*width)+(w+width_written)] = bits % 2;
			}
		}
		width_written += char_width*scaling_factor;
	}
	return arr;
}

int get_char_width(font_descriptor_t* fdes, int ch) {
	int width = 0;
	if ((ch >= fdes->firstchar) && (ch-fdes->firstchar < fdes->size)) {
		ch -= fdes->firstchar;
		if (!fdes->width) {
			width = fdes->maxwidth;
		} else {
			width = fdes->width[ch];
		}
	}
	return width;
}
