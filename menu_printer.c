/*
 * Drawing menu for user input.
 * author: Dusan Jansky, Martin Havlicek
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "struct.h"
#include "task_processor.h"

int menu_val = 0;

void clear_lines(int height) {
	for (int i = 0; i < height; i++) {
		printf("\n");
	}
	return;
}

void print_module_menu() {
	const int number_of_options = 7;

	printf("\033[11A");
	printf("\033[K\n");
	printf("\033[KSelect module for interation:\n");
	printf("\033[K\033[0;31m1\033[0;0m: Reactor Controller\n");
	printf("\033[K\033[0;31m2\033[0;0m: Oxygen Generator\n");
	printf("\033[K\033[0;31m3\033[0;0m: Carbon Dioxide Scrubber\n");
	printf("\033[K\033[0;31m4\033[0;0m: Tempeture Manager\n");
	printf("\033[K\033[0;31m5\033[0;0m: Pressure Stabilizer\n");
	printf("\033[K\033[0;31m6\033[0;0m: Airlock Controller\n");
	printf("\033[KPress \033[0;31mQ\033[0;0m for ending game.\n");

	for (int i = 0; i < 9-number_of_options; i++) {
		printf("\033[K\n");
	}

	return;
}

void print_core_menu() {
	const int number_of_options = 3;

	printf("\033[11A");
	printf("\033[K\n");
	printf("\033[KChange reactor settings:\n");
	printf("\033[K\033[0;31m1\033[0;0m: Set to maximum power output\n");
	printf("\033[K\033[0;31m2\033[0;0m: Set to minimum power output\n");
	printf("\033[KPress \033[0;31mB\033[0;0m for return.\n");

	for (int i = 0; i < 9-number_of_options; i++) {
		printf("\033[K\n");
	}

	return;
}

void print_o2_menu() {
	const int number_of_options = 4;

	printf("\033[11A");
	printf("\033[K\n");
	printf("\033[KOxygen generator settings:\n");
	printf("\033[K\033[0;31m1\033[0;0m: Release oxygen\n");
	printf("\033[K\033[0;31m2\033[0;0m: Store oxygen\n");
	printf("\033[K\033[0;31m3\033[0;0m: Stop releasing or storing\n");
	printf("\033[KPress \033[0;31mB\033[0;0m for return.\n");

	for (int i = 0; i < 9-number_of_options; i++) {
		printf("\033[K\n");
	}

	return;
}

void print_co2_menu() {
	const int number_of_options = 4;

	printf("\033[11A");
	printf("\033[K\n");
	printf("\033[KCarbon dioxide scrubber settings:\n");
	printf("\033[K\033[0;31m1\033[0;0m: Release carbon dioxide \033[0;33m(Only in case of emergency)\033[0;0m\n");
	printf("\033[K\033[0;31m2\033[0;0m: Store carbon dioxide\n");
	printf("\033[K\033[0;31m3\033[0;0m: Stop releasing or storing\n");
	printf("\033[KPress \033[0;31mB\033[0;0m for return.\n");

	for (int i = 0; i < 9-number_of_options; i++) {
		printf("\033[K\n");
	}

	return;
}

void print_temp_menu() {
	const int number_of_options = 4;

	printf("\033[11A");
	printf("\033[K\n");
	printf("\033[KTemperature regulator settings:\n");
	printf("\033[K\033[0;31m1\033[0;0m: Turn on heater\n");
	printf("\033[K\033[0;31m2\033[0;0m: Turn on cooler\n");
	printf("\033[K\033[0;31m3\033[0;0m: Turn off heating or cooling\n");
	printf("\033[KPress \033[0;31mB\033[0;0m for return.\n");

	for (int i = 0; i < 9-number_of_options; i++) {
		printf("\033[K\n");
	}

	return;
}

void print_press_menu() {
	const int number_of_options = 4;

	printf("\033[11A");
	printf("\033[K\n");
	printf("\033[KPressure regulator settings:\n");
	printf("\033[K\033[0;31m1\033[0;0m: Release nitrogen\n");
	printf("\033[K\033[0;31m2\033[0;0m: Store nitrogen\n");
	printf("\033[K\033[0;31m3\033[0;0m: Stop releasing or storing\n");
	printf("\033[KPress \033[0;31mB\033[0;0m for return.\n");

	for (int i = 0; i < 9-number_of_options; i++) {
		printf("\033[K\n");
	}

	return;
}

void print_airlock_menu() {
	const int number_of_options = 3;

	printf("\033[11A");
	printf("\033[K\n");
	printf("\033[KAirlock door control:\n");
	printf("\033[K\033[0;31m1\033[0;0m: Open airlock \033[0;33m(Only in case of emergency)\033[0;0m\n");
	printf("\033[K\033[0;31m2\033[0;0m: Close airlock\n");
	printf("\033[KPress \033[0;31mB\033[0;0m for return.\n");

	for (int i = 0; i < 9-number_of_options; i++) {
		printf("\033[K\n");
	}

	return;
}

void print_menu() {
	switch(menu_val) {
		case 0:
			print_module_menu();
			break;
		case 1:
			print_core_menu();
			break;
		case 2:
			print_o2_menu();
			break;
		case 3:
			print_co2_menu();
			break;
		case 4:
			print_temp_menu();
			break;
		case 5:
			print_press_menu();
			break;
		case 6:
			print_airlock_menu();
			break;
		default:
			break;
	}

	return;
}

void update_menu(data_t data, char input) {
	switch(menu_val) {


		// module menu
		case 0:
			switch(input) {
				case '1':
					menu_val = 1;
					break;
				case '2':
					menu_val = 2;
					break;
				case '3':
					menu_val = 3;
					break;
				case '4':
					menu_val = 4;
					break;
				case '5':
					menu_val = 5;
					break;
				case '6':
					menu_val = 6;
					break;
				default:
					break;
			}
			break;


		// core menu
		case 1:
			switch(input) {
				case '1':
				case '2':
					set_device(data, menu_val, select_status(input));
					break;
				case 'b':
					menu_val = 0;
					break;
				default:
					break;
			}
			break;


		// o2 menu
		case 2:
			switch(input) {
				case '1':
				case '2':
				case '3':
					set_device(data, menu_val, select_status(input));
					break;
				case 'b':
					menu_val = 0;
					break;
				default:
					break;
			}
			break;


		// co2 menu
		case 3:
			switch(input) {
				case '1':
				case '2':
				case '3':
					set_device(data, menu_val, select_status(input));
					break;
				case 'b':
					menu_val = 0;
					break;
				default:
					break;
			}
			break;


		// temp menu
		case 4:
			switch(input) {
				case '1':
				case '2':
				case '3':
					set_device(data, menu_val, select_status(input));
					break;
				case 'b':
					menu_val = 0;
					break;
				default:
					break;
			}
			break;


		// press menu
		case 5:
			switch(input) {
				case '1':
				case '2':
				case '3':
					set_device(data, menu_val, select_status(input));
					break;
				case 'b':
					menu_val = 0;
					break;
				default:
					break;
			}
			break;


		// airlock menu
		case 6:
			switch(input) {
				case '1':
				case '2':
					set_device(data, menu_val, select_status(input));
					break;
				case 'b':
					menu_val = 0;
					break;
				default:
					break;
			}
			break;


		default:
			break;
	}

	return;
}
