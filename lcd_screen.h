/*
 * Basic drawing composition of larger elements on lcd
 * authors: Dusan Jansky, Martin havlicek
 */

#ifndef LCD_SCREEN_HEADER
#define LCD_SCREEN_HEADER

#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>

#include "font_types.h"

/*
 * clear display
 */
void set_screen_black(unsigned char *parlcd_mem_base);

/*
 * get pointer to font array
 */
bool* get_string_field(double scaling_factor, font_descriptor_t* fdes, char *str, int *width);

/*
 * redraw whole lcd based on parametres
 */
void draw_whole_lcd(unsigned char* parlcd_mem_base, data_t* values);

/*
 * returning font character width
 */
int get_char_width(font_descriptor_t* fdes, int ch);

#endif
