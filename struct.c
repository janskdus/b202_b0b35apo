/*
 * Sets global parameters data structure
 * authors: Dusan Jansky, Martin havlicek
 */

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#include "struct.h"


/*
 * inits structs for in game mode
 */
data_t set_data() {

	//displayed values
	displayed_vals_t *values = malloc(6 * sizeof(displayed_vals_t));
	double value[] = {27.7, 21.5, 1.1, 21.5, 1.3, 77.2};
	double min[] = {0, 10, 0, 0, 0, 0};
	double max[] = {100, 50, 3, 100, 10, 100};
	for (int i = 0; i < 6; i++) {
		values[i].value = value[i];
		values[i].min = min[i];
		values[i].max = max[i];
	}

	windows_t *win = malloc(8 * sizeof(windows_t));
	
	//window names
	win[0].name = "amp";
	win[1].name = "temp";
	win[2].name = "press";
	win[3].name = "o2";
	win[4].name = "co2";
	win[5].name = "N";
	win[6].name = "console";
	win[7].name = "dead overlay";

	//windows types
	//	1 = square_meter
	//	2 = coll_meter
	//	3 = console
	win[0].type = 1;
	win[1].type = 1;
	win[2].type = 1;
	win[3].type = 2;
	win[4].type = 2;
	win[5].type = 2;
	win[6].type = 3;
	win[7].type = 4;

	// square modules
	for (int i = 0; i<3;i++) {
		win[i].cnf.start_x = 4+i*104;
		win[i].cnf.start_y = 4;
		win[i].cnf.width = 100;
		win[i].cnf.height = 100;
	}

	// coll modules
	for (int i = 0; i<3;i++) {
		win[i+3].cnf.start_x = 316+i*54;
		win[i+3].cnf.start_y = 4;
		win[i+3].cnf.width = 50;
		win[i+3].cnf.height = 312;
	}

	// console
	win[6].cnf.start_x = 4;
	win[6].cnf.start_y = 108;
	win[6].cnf.width = 308;
	win[6].cnf.height = 208;

	// dead overlay
	win[7].cnf.start_x = 0;
	win[7].cnf.start_y = 107;
	win[7].cnf.width = 480;
	win[7].cnf.height = 5*16+25;

	message_t *msg = malloc(7 * sizeof(message_t));

	for (int i = 0; i < 7; i++) {
		msg[i].string = malloc(20 * sizeof(char));
		//msg[i].string = "aaaaabbbbbcccccddddd";
		msg[i].color = 0x0;
	}

	data_t data = {
		malloc(320*480*sizeof(uint16_t)),
		values,
		{
			msg,
			.len = 20,
			.rows = 7,
			.text_size = 2,
		},
		win,
		0,
		0,
	};
	return data;
}
