/*
 * Printing infomation code messages to console and display
 * author: Dusan Jansky, Martin Havlicek
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "struct.h"
#include "menu_printer.h"

const char splitter[] = ": ";

const char modules[7][7] = {
	"0Bi-1",
	"AZ-5",
	"GxO2",
	"GxCO",
	"T-H0T",
	"B4Rman",
	"h0D0R",
};

const char messages[][7] = {
	"A0Fn23",
	"A0En45",
	"A0Sf31",
	"A0BBH4",
	"A0Af87",
	"A0OSpg",

	"N1N4-1",
	"N1N4-0",
	"BWP-00",
	"C0LDAF",
	"H0WD3R",

	"1C4NoX",
	"1C4Noo",
	"1C4NoS",
	"L00XS1",
	"L00XN0",
	"N00XAM",
	"N00XFY",

	"D1ZZ-1",
	"D1ZZ-0",
	"D1ZZ-S",
	"h3cnBR",
	"D1EAlr",
	"StND3d",

	"TW1TCH",
	"TW0TCH",
	"TWSTCH",
	"1C3-BY",
	"H3LLN0",
	"N0M4N4",
	"STM4N4",

	"L3TS-S",
	"PLSC0A",
	"NITROS",
	"S4CK3D",
	"N0R00M",
	"GBmyFr",
	"WDYW4T",

	"h0D0R!",
	"H0LDTD",
};

const int msg_colors[] = {
	0,
	0,
	0,
	0,
	0,
	0,

	0,
	0,
	1,
	1,
	2,

	0,
	0,
	0,
	1,
	1,
	2,
	2,

	0,
	0,
	0,
	1,
	2,
	2,

	0,
	0,
	0,
	1,
	1,
	2,
	2,

	0,
	0,
	0,
	1,
	1,
	2,
	2,

	0,
	0,
};

void print_console(char *string, uint16_t color, console_t con) {
	for (int i = con.rows - 2; i >= 0; i--) {
		for (int w = 0; w < con.len; w++) {
			con.msg[i+1].string[w] = con.msg[i].string[w];
		}
		con.msg[i+1].color = con.msg[i].color;
	}

	for (int i = 0; i < con.len; i++) {
		if (i < strlen(string)) {
			con.msg[0].string[i] = string[i];
		} else {
			con.msg[0].string[i] = ' ';
		}
	}
	con.msg[0].color = color;

	return;
}

void print_msg(char *string, bool enable_color, int color) {
	printf("\33[12A");

	if (enable_color) {
		if (color == 1) printf("\33[0;33m");
		else if (color == 2) printf("\33[0;31m");
	}

	printf("\33[K%s\n", string);
	printf("\33[0;0m");
	printf("\33[K");
	printf("\33[11B\n");

	print_menu();

	return;
}

void print_code(int module_id, int msg_id, bool draw_cons, data_t data) {
	char *string = malloc(sizeof(modules[module_id])+sizeof(splitter)+sizeof(messages[msg_id]));

	strcat(string, modules[module_id]);
	strcat(string, splitter);
	strcat(string, messages[msg_id]);

	print_msg(string, true, msg_colors[msg_id]);

	if (draw_cons) {
		uint16_t color;

		if (msg_colors[msg_id] == 0) {
			color = 0xffff;
		} else if (msg_colors[msg_id] == 1) {
			color = 0xfde0;
		} else if (msg_colors[msg_id] == 2) {
			color = 0xf800;
		} else {
			color = 0xffff;
		}

		print_console(string, color, data.console);
	}

	free(string);

	return;
}
