/*******************************************************************
  Project main function file made for semestral work of APO
  subject in 2021.

  main.c      - main file

Authors: Dušan Jánsky, Martin Havlíček
License: GNU GPLv3

 *******************************************************************/

#define _POSIX_C_SOURCE 200112L

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <termios.h>
#include <math.h>

#include "macros.h"
#include "struct.h"
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "serialize_lock.h"
#include "font_types.h"
#include "lcd_screen.h"
#include "modules_game.h"
#include "messages.h"
#include "menu_printer.h"
#include "task_processor.h"
#include "periferies.h"
#include "events.h"

#define EVENT_TIME 600

int main(int argc, char *argv[])
{
	data_t data = set_data();

	uint32_t val_line=1;
	unsigned char *mem_base;
	unsigned char *parlcd_mem_base;
	static struct termios oldt, newt;
	bool quit = false;
	char ch1=' ';

	tcgetattr( STDIN_FILENO, &oldt);
	newt = oldt;

	newt.c_lflag &= ~(ICANON);
	newt.c_cc[VMIN] = 0; // bytes until read unblocks.
	newt.c_cc[VTIME] = 0;

	tcsetattr( STDIN_FILENO, TCSANOW, &newt);

	printf("Main loop starting...\n");
	clear_lines(20);

	mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);

	if (mem_base == NULL)
		exit(1);

	parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
	parlcd_hx8357_init(parlcd_mem_base);
	set_screen_black(mem_base);

	// TASK PROCESSOR
	set_default_values(&data);

	struct timespec loop_delay;
	loop_delay.tv_sec = 0;
	loop_delay.tv_nsec = 100 * 1000 * 1000;
	int duration = EVENT_TIME; // time to next event (120 sec)
	

	while (!quit) {
		int r = read(0, &ch1, 1);

		if (r==0) {
			print_menu();
		}

		while (r==1) {
			if (ch1=='q') {
				quit = true;
				break;
			}

			update_menu(data,ch1);

			r = read(0, &ch1, 1);
		}

		update_led_bar(mem_base, &val_line);
		update_values(&data);

		data.life_cycles++;

		// Start new event 
		if (data.life_cycles > duration) {
			init_events(data);
			set_event(rand_event(), data);
			duration *= 0.95;
			do_event_black_hole();
			duration += EVENT_TIME;// run random event
		}
		
		if (!alive()){
			data.dead = 1;
			quit = true;
		}

		draw_whole_lcd(parlcd_mem_base, &data);

		clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL);
	}

	print_alive_time(data.life_cycles);
	printf("Press anything for end.\n");
	
	int r = read(0, &ch1, 1);
	while (r==0) {
		r = read(0, &ch1, 1);
	}

	// clearing board
	clear_board(mem_base, parlcd_mem_base);

	tcsetattr( STDIN_FILENO, TCSANOW, &oldt);

	printf("Main loop ending...\n");

	return 0;
}
