/*
 * Functions for periferies without lcd
 * author: Dusan Jansky, Martin havlicek
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "periferies.h"

void clear_board(unsigned char *mem_base, unsigned char *parlcd_mem_base) {
	set_screen_black(parlcd_mem_base);
	*(volatile uint32_t*)(mem_base + SPILED_REG_LED_LINE_o) = 0x0;
	*(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB1_o) = 0x0;
	*(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB2_o) = 0x0;
	
	return;
}

void update_led_bar(unsigned char *mem_base, uint32_t *val_line) {
	*val_line>>=1;
	*val_line+=0x80000000;

	if(*val_line==0xFFFFFFFF) *val_line = 0xE0000000;

	*(volatile uint32_t*)(mem_base + SPILED_REG_LED_LINE_o) = *val_line;

	return;
}
