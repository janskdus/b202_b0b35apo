/*
 * Functions for periferies without lcd
 * author: Dusan Jansky, Martin havlicek
 */

#ifndef PERIFERIES_HEADER
#define PERIFERIES_HEADER

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "macros.h"
#include "struct.h"
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "serialize_lock.h"
#include "font_types.h"
#include "lcd_screen.h"
#include "modules_game.h"
#include "messages.h"
#include "menu_printer.h"
#include "task_processor.h"

/*
 * reset led diods 
 */
void clear_board(unsigned char *mem_base, unsigned char *parlcd_mem_base);

/*
 * With each call modify number of turned on leds
 */
void update_led_bar(unsigned char *mem_base, uint32_t *val_line);

#endif
