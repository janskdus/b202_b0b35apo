/*
 * Events management
 * author: Dusan Jansky, Martin Havlicek
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "events.h"
#include "struct.h"
#include "macros.h"
#include "task_processor.h"
#include "lcd_screen.h"
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "serialize_lock.h"
#include "font_types.h"
#include "messages.h"


bool* events;
int num_of_events = 4;

enum events_enum { NEBULA_ICE, NEBULA_ELC, STAR, BLACK_HOLE };

void init_events(data_t data){
	if (events == NULL)
		events = malloc(sizeof(bool)*num_of_events);
	else
		print_code(0, 5, true, data);

	if (!events){
		perror("Alloc error events: ");
		exit(EXIT_FAILURE);
	}
	for (int i = 0; i < num_of_events; ++i)
		events[i] = false;
}

int rand_event(){
	return (rand() % num_of_events);
}

void set_event(int idx, data_t data){
	if (idx < num_of_events && idx >= 0){ 
		events[idx] = true;
		switch(idx){
			case NEBULA_ICE:
				print_code(0, 1, true, data);
				break;
			case NEBULA_ELC:
				print_code(0, 2, true, data);
				break;
			case STAR:
				print_code(0, 3, true, data);
				break;
			case BLACK_HOLE:
				print_code(0, 4, true, data);
				break;
		}
	} else {
		fprintf(stderr, "event index in 'set_event' out of range.\n");
		exit(EXIT_FAILURE);
	}
}

void print_alive_time(unsigned long int cycles) {
	int alive_time_mS = cycles % 10;
	int alive_time_H = cycles / 36000;
	cycles -= alive_time_H * 36000;
	int alive_time_M = cycles / 600;
	cycles -= alive_time_M * 600;
	int alive_time_S = cycles / 10;

	printf("\nYou were alive for %02d:%02d:%02d.%01d.\n", alive_time_H, alive_time_M, alive_time_S, alive_time_mS);
	return;
}

void process_event(int idx){
	switch(idx){
		case NEBULA_ICE:
			do_event_nebula_ice();
			break;
		case NEBULA_ELC:
			do_event_nebula_elc();
			break;
		case STAR:
			do_event_star();
			break;
		case BLACK_HOLE:
			do_event_black_hole();
			break;
		default:
			// unknown index
			break;
	}
}

