/*
 * Events management
 * author: Dusan Jansky, Martin Havlicek
 */

#ifndef EVENTS_HEADER
#define EVENTS_HEADER

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>


#include "events.h"
#include "struct.h"
#include "macros.h"
#include "task_processor.h"
#include "lcd_screen.h"
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "serialize_lock.h"
#include "font_types.h"

/*
 * alloc memory for events and set events false
 */
void init_events(data_t data);

/*
 * return radnom idx number <0, 'number of events'>
 */
int rand_event();

/*
 * Turn on event at index
 */
void set_event(int idx, data_t data);

/*
 * prints time alive to cmd
 */
void print_alive_time(unsigned long int cycles);

/*
 * process all events which are set 'true'
 * should be called periodicaly
 * calls functions, which process individual events
 */
void process_event(int idx);


#endif
